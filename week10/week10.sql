use company;

select count(CustomerID),country
from customers
group by country
order by count(CustomerID) desc;

create view usa_customers as
select CustomerName
from customers
where Country = "USA";

select *
from usa_customers;

create or replace view products_over_avg as
select ProductName, Price
from products
where Price >  (select avg(Price) from products);




