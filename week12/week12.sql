use movie_db;

load data 
infile " C:\\Users\\dilan\\Desktop\\ denormalized.csv "
into table denormalized
columns terminated by ';';

show variables like "secure_file_priv";

select movie_id,title,ranking,rating,year,votes,duration,oscars,budget
from denormalized;

insert into movies(movie_id,title,ranking,rating,year,votes,duration,oscars,budget)
select distinct movie_id,title,ranking,rating,year,votes,duration,oscars,budget
from denormalized;

select *from movies